﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Configuration;

namespace DAL
{
    public class PAMDL
    {        
        private string strcon = ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
                             
        public string AddBulletintoDB(string Title, string CreatedBy)
        {
            SqlConnection DbConnection = new SqlConnection(strcon);

            try
            {
                DbConnection.Open();

                SqlCommand command = new SqlCommand("[PAM].[usp_AddBulletin]", DbConnection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@Title", SqlDbType.NVarChar, 512).Value = Title;
                command.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 30).Value = CreatedBy;

                SqlParameter returnParameter = command.Parameters.Add("RetVal", SqlDbType.Int);
                returnParameter.Direction = ParameterDirection.ReturnValue;

                int i = command.ExecuteNonQuery();
                int retValue = (int)returnParameter.Value;

                DbConnection.Close();
                if (i >= 1)
                {
                    return "Bulletin added successfully";
                }
                else if (retValue == -1)
                {
                    return "Bulletin Title already exists";
                }
                else
                {
                    return "Bulletin could not be added";
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
        }
        
        public DataSet GetAllUsers()
        {
            SqlConnection DbConnection = new SqlConnection(strcon);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();

            try
            {
                DbConnection.Open();

                SqlCommand command = new SqlCommand("[PAM].[usp_GetAllUsers]", DbConnection);
                command.CommandType = CommandType.StoredProcedure;
                
                da.SelectCommand = command;
                da.Fill(ds);

                DbConnection.Close();
                return ds;
            }
            catch (SqlException ex)
            {
                throw;
            }
        }
                
        public DataSet GetUsersByEmail(string emailID, string role)
        {
            //BulletinGet bulletin = new BulletinGet { };
            SqlConnection DbConnection = new SqlConnection(strcon);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();

            try
            {
                DbConnection.Open();

                SqlCommand command = new SqlCommand("[PAM].[usp_GetUsersByEmail]", DbConnection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@Email", SqlDbType.NVarChar, 100).Value = emailID; 
                command.Parameters.Add("@Role", SqlDbType.VarChar, 30).Value = role; 
                da.SelectCommand = command;
                da.Fill(ds);

                DbConnection.Close();

                return ds;
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        public DataSet GetUsersByName(string FName, string LName, string role)
        {
            //BulletinGet bulletin = new BulletinGet { };
            SqlConnection DbConnection = new SqlConnection(strcon);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();

            try
            {
                DbConnection.Open();

                SqlCommand command = new SqlCommand("[PAM].[usp_GetUsersByName]", DbConnection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@FirstName", SqlDbType.NVarChar, 50).Value = FName;
                command.Parameters.Add("@LastName", SqlDbType.NVarChar, 50).Value = LName;
                command.Parameters.Add("@Role", SqlDbType.VarChar, 30).Value = role;
                da.SelectCommand = command;
                da.Fill(ds);

                DbConnection.Close();

                return ds;
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        public DataSet GetUsersByAgency(string AgencyName, string role)
        {
            //BulletinGet bulletin = new BulletinGet { };
            SqlConnection DbConnection = new SqlConnection(strcon);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();

            try
            {
                DbConnection.Open();

                SqlCommand command = new SqlCommand("[PAM].[usp_GetUsersByAgency]", DbConnection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@AgencyName", SqlDbType.NVarChar, 200).Value = AgencyName;
                command.Parameters.Add("@Role", SqlDbType.VarChar, 30).Value = role;
                da.SelectCommand = command;
                da.Fill(ds);

                DbConnection.Close();

                return ds;
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        public DataSet GetUsersByProducerID(string ProducerID, string role)
        {
            //BulletinGet bulletin = new BulletinGet { };
            SqlConnection DbConnection = new SqlConnection(strcon);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();

            try
            {
                DbConnection.Open();

                SqlCommand command = new SqlCommand("[PAM].[usp_GetUsersByProducerID]", DbConnection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@producerID", SqlDbType.NVarChar, 200).Value = ProducerID;
                command.Parameters.Add("@Role", SqlDbType.VarChar, 30).Value = role;
                da.SelectCommand = command;
                da.Fill(ds);

                DbConnection.Close();

                return ds;
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        public DataSet GetUser(int ID)
        {
            SqlConnection DbConnection = new SqlConnection(strcon);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();

            try
            {
                DbConnection.Open();

                SqlCommand command = new SqlCommand("[PAM].[usp_GetUser]", DbConnection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@ID", SqlDbType.Int).Value = ID;

                da.SelectCommand = command;
                da.Fill(ds);

                DbConnection.Close();
                return ds;
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        public DataSet GetUserbyEmail(string emailID)
        {
            SqlConnection DbConnection = new SqlConnection(strcon);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();

            try
            {
                DbConnection.Open();

                SqlCommand command = new SqlCommand("[PAM].[usp_GetUserbyEmail]", DbConnection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@emailID", SqlDbType.NVarChar, 100).Value = emailID;

                da.SelectCommand = command;
                da.Fill(ds);

                DbConnection.Close();
                return ds;
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        public DataSet GetProducersForAdmin(string emailID)
        {
            SqlConnection DbConnection = new SqlConnection(strcon);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();

            try
            {
                DbConnection.Open();

                SqlCommand command = new SqlCommand("[PAM].[usp_GetProducersForAdmin]", DbConnection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@emailID", SqlDbType.NVarChar, 100).Value = emailID;

                da.SelectCommand = command;
                da.Fill(ds);

                DbConnection.Close();
                return ds;
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        public string AddAdministratortoDB(string FirstName, string LastName, string EmailID, DateTime DOB, string AgencyName, int Commission_Stmt
                                            , bool IsInHongKong, bool IsInSingapore, bool IsInBermuda, string CreatedBy, DataTable ProducerAdmMappingTable)
        {
            SqlConnection DbConnection = new SqlConnection(strcon);

            try
            {
                DbConnection.Open();

                SqlCommand command = new SqlCommand("[PAM].[usp_AddAdministrator]", DbConnection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@FirstName", SqlDbType.NVarChar, 50).Value = FirstName;
                command.Parameters.Add("@LastName", SqlDbType.NVarChar, 50).Value = LastName;
                command.Parameters.Add("@EmailID", SqlDbType.NVarChar, 100).Value = EmailID;
                command.Parameters.Add("@DOB", SqlDbType.NVarChar, 100).Value = DOB;
                command.Parameters.Add("@AgencyName", SqlDbType.NVarChar, 200).Value = AgencyName;
                command.Parameters.Add("@Commission_Stmt", SqlDbType.NVarChar, 5).Value = Commission_Stmt;
                command.Parameters.Add("@IsInHongKong", SqlDbType.Bit).Value = IsInHongKong;
                command.Parameters.Add("@IsInSingapore", SqlDbType.Bit).Value = IsInSingapore;
                command.Parameters.Add("@IsInBermuda", SqlDbType.Bit).Value = IsInBermuda;
                command.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 30).Value = CreatedBy;
                command.Parameters.Add("@ProdAdm", SqlDbType.Structured).Value = ProducerAdmMappingTable;

                SqlParameter returnParameter = command.Parameters.Add("RetVal", SqlDbType.Int);
                returnParameter.Direction = ParameterDirection.ReturnValue;

                int i = command.ExecuteNonQuery();
                int retValue = (int)returnParameter.Value;

                DbConnection.Close();

                if (i >= 1)
                {
                    return "Administrator added successfully";
                }
                else if (retValue == -1)
                {
                    return "User with same EmailID already exists";
                }
                else
                {
                    return "Administrator could not be added";
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        public string AddTLBStafftoDB(string FirstName, string LastName, string EmailID, DateTime DOB, int Commission_Stmt
                                            , bool IsInHongKong, bool IsInSingapore, bool IsInBermuda, string CreatedBy)
        {
            SqlConnection DbConnection = new SqlConnection(strcon);

            try
            {
                DbConnection.Open();

                SqlCommand command = new SqlCommand("[PAM].[usp_AddTLBStaff]", DbConnection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@FirstName", SqlDbType.NVarChar, 50).Value = FirstName;
                command.Parameters.Add("@LastName", SqlDbType.NVarChar, 50).Value = LastName;
                command.Parameters.Add("@EmailID", SqlDbType.NVarChar, 100).Value = EmailID;
                command.Parameters.Add("@DOB", SqlDbType.NVarChar, 100).Value = DOB;
                command.Parameters.Add("@Commission_Stmt", SqlDbType.NVarChar, 5).Value = Commission_Stmt;
                command.Parameters.Add("@IsInHongKong", SqlDbType.Bit).Value = IsInHongKong;
                command.Parameters.Add("@IsInSingapore", SqlDbType.Bit).Value = IsInSingapore;
                command.Parameters.Add("@IsInBermuda", SqlDbType.Bit).Value = IsInBermuda;
                command.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 30).Value = CreatedBy;

                SqlParameter returnParameter = command.Parameters.Add("RetVal", SqlDbType.Int);
                returnParameter.Direction = ParameterDirection.ReturnValue;

                int i = command.ExecuteNonQuery();
                int retValue = (int)returnParameter.Value;

                DbConnection.Close();

                if (i >= 1)
                {
                    return "TLBStaff added successfully";
                }
                else if (retValue == -1)
                {
                    return "User with same EmailID already exists";
                }
                else
                {
                    return "TLBStaff could not be added";
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        public string UpdateUsertoDB(int ID, int Commission_Stmt, bool IsInHongKong, bool IsInSingapore, bool IsInBermuda, bool IsActive)
        {
            SqlConnection DbConnection = new SqlConnection(strcon);

            try
            {
                DbConnection.Open();

                SqlCommand command = new SqlCommand("[PAM].[usp_UpdateUser]", DbConnection);
                command.CommandType = CommandType.StoredProcedure;
                
                command.Parameters.Add("@ID", SqlDbType.Int).Value = ID;
                command.Parameters.Add("@Commission_Stmt", SqlDbType.Int).Value = Commission_Stmt;
                command.Parameters.Add("@IsInHongKong", SqlDbType.Bit).Value = IsInHongKong;
                command.Parameters.Add("@IsInSingapore", SqlDbType.Bit).Value = IsInSingapore;
                command.Parameters.Add("@IsInBermuda", SqlDbType.Bit).Value = IsInBermuda;
                command.Parameters.Add("@IsActive", SqlDbType.Bit).Value = IsActive;

                SqlParameter returnParameter = command.Parameters.Add("RetVal", SqlDbType.Int);
                returnParameter.Direction = ParameterDirection.ReturnValue;

                int i = command.ExecuteNonQuery();
                int retValue = (int)returnParameter.Value;

                DbConnection.Close();

                if (i >= 1)
                {
                    return "User updated successfully";
                }
                else
                {
                    return "User could not be updated";
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        //public string UpdateTLBStafftoDB(int TLBStaffID, string Commission_Stmt, bool IsInHongKong, bool IsInSingapore, bool IsInBermuda, bool IsActive)
        //{
        //    SqlConnection DbConnection = new SqlConnection(strcon);

        //    try
        //    {
        //        DbConnection.Open();

        //        SqlCommand command = new SqlCommand("[PAM].[usp_UpdateTLBStaff]", DbConnection);
        //        command.CommandType = CommandType.StoredProcedure;
                
        //        command.Parameters.Add("@TLBStaffID", SqlDbType.Int).Value = TLBStaffID;
        //        command.Parameters.Add("@Commission_Stmt", SqlDbType.NVarChar, 5).Value = Commission_Stmt;
        //        command.Parameters.Add("@IsInHongKong", SqlDbType.Bit).Value = IsInHongKong;
        //        command.Parameters.Add("@IsInSingapore", SqlDbType.Bit).Value = IsInSingapore;
        //        command.Parameters.Add("@IsInBermuda", SqlDbType.Bit).Value = IsInBermuda;
        //        command.Parameters.Add("@IsActive", SqlDbType.Bit).Value = IsActive;

        //        SqlParameter returnParameter = command.Parameters.Add("RetVal", SqlDbType.Int);
        //        returnParameter.Direction = ParameterDirection.ReturnValue;

        //        int i = command.ExecuteNonQuery();
        //        int retValue = (int)returnParameter.Value;

        //        DbConnection.Close();

        //        if (i >= 1)
        //        {
        //            return "TLBStaff updated successfully";
        //        }
        //        else
        //        {
        //            return "TLBStaff could not be updated";
        //        }
        //    }
        //    catch (SqlException ex)
        //    {
        //        throw;
        //    }
        //}

        public string UpdateProducerAdminMappingtoDB(DataTable ProducerAdmMappingTable)
        {
            SqlConnection DbConnection = new SqlConnection(strcon);

            try
            {
                DbConnection.Open();

                SqlCommand command = new SqlCommand("[PAM].[usp_UpdateProducers]", DbConnection);
                command.CommandType = CommandType.StoredProcedure;

                //command.Parameters.Add("@AdministratorID", SqlDbType.Int).Value = AdministratorID;
                //command.Parameters.Add("@ProducerID", SqlDbType.NVarChar, 20).Value = ProducerID;
                //command.Parameters.Add("@FirstName", SqlDbType.NVarChar, 50).Value = FirstName;
                //command.Parameters.Add("@LastName", SqlDbType.NVarChar, 50).Value = LastName;
                //command.Parameters.Add("@IsActive", SqlDbType.Bit).Value = IsActive;
                command.Parameters.Add("@ProdAdm", SqlDbType.Structured).Value = ProducerAdmMappingTable;
                
                SqlParameter returnParameter = command.Parameters.Add("RetVal", SqlDbType.Int);
                returnParameter.Direction = ParameterDirection.ReturnValue;

                int i = command.ExecuteNonQuery();
                int retValue = (int)returnParameter.Value;

                DbConnection.Close();

                if (i >= 1)
                {
                    return "Producer(s) updated successfully";
                }
                else
                {
                    return "Producer(s) could not be updated";
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
        }
    }
}