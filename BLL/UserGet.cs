﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BLL
{
    public class UserGet
    {
        public int ID { get; set; }
        public string FullName { get; set; }
        public string EmailID { get; set; }
        public DateTime DOB { get; set; }
        public string Role { get; set; }
        public string ProducerID { get; set; }
        public string AgencyName { get; set; }
        public DateTime LastLoginDate { get; set; }
        public string Status { get; set; }

    }
}