﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BLL
{
    public class UserAccessControl
    {
        public int ID { get; set; }
        public string Role { get; set; }
        public int Commission_Stmt { get; set; }
        public bool IsInHongKong { get; set; }
        public bool IsInSingapore { get; set; }
        public bool IsInBermuda { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
    }
}
