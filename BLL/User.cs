﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BLL
{
    public class User
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailID { get; set; }
        public DateTime DOB { get; set; }
        public string Role { get; set; }
        public string ProducerID { get; set; }
        public string Country { get; set; }
        public string AgencyName { get; set; }
        public int Commission_Stmt { get; set; }
        public bool IsInHongKong { get; set; }
        public bool IsInSingapore { get; set; }
        public bool IsInBermuda { get; set; }
        public DateTime LastLoginDate { get; set; }
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }

        public List<ProducerAdmMapping> ProducerAdm { get; set; }
    }
}