﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BLL
{
    public class ProducerAdmMapping
    {
        public int AdministratorID { get; set; }
        public string ProducerID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsActive { get; set; }
    }
}