﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class UserMethods
    {
        public List<UserGet> GetAllUsers()
        {
            PAMDL DBObj = new PAMDL();
            Helper hlpObj = new Helper();
            List<UserGet> userList = new List<UserGet>();

            using (DataSet ds = DBObj.GetAllUsers())
            {
                if (ds.Tables.Count > 0)
                    // Map data to UserGet class using DataTableMapToList
                    userList = hlpObj.ConvertDataTable<UserGet>(ds.Tables[0]).ToList();
            }
            return userList;
        }

        public List<UserGet> GetUsersByEmail(string emailID, string role)
        {
            PAMDL DBObj = new PAMDL();
            Helper hlpObj = new Helper();
            List<UserGet> userList = new List<UserGet>();

            using (DataSet ds = DBObj.GetUsersByEmail(emailID, role))
            {
                if (ds.Tables.Count > 0)
                    // Map data to UserGet class using DataTableMapToList
                    userList = hlpObj.ConvertDataTable<UserGet>(ds.Tables[0]).ToList();
            }
            return userList;
        }

        public List<UserGet> GetUsersByName(string FName, string LName, string role)
        {
            PAMDL DBObj = new PAMDL();
            Helper hlpObj = new Helper();
            List<UserGet> userList = new List<UserGet>();

            using (DataSet ds = DBObj.GetUsersByName(FName, LName, role))
            {
                if (ds.Tables.Count > 0)
                    // Map data to UserGet class using DataTableMapToList
                    userList = hlpObj.ConvertDataTable<UserGet>(ds.Tables[0]).ToList();
            }
            return userList;
        }

        public List<UserGet> GetUsersByAgency(string AgencyName, string role)
        {
            PAMDL DBObj = new PAMDL();
            Helper hlpObj = new Helper();
            List<UserGet> userList = new List<UserGet>();

            using (DataSet ds = DBObj.GetUsersByAgency(AgencyName, role))
            {
                if (ds.Tables.Count > 0)
                    // Map data to UserGet class using DataTableMapToList
                    userList = hlpObj.ConvertDataTable<UserGet>(ds.Tables[0]).ToList();
            }
            return userList;
        }

        public List<UserGet> GetUsersByProducerID(string ProducerID, string role)
        {
            PAMDL DBObj = new PAMDL();
            Helper hlpObj = new Helper();
            List<UserGet> userList = new List<UserGet>();

            using (DataSet ds = DBObj.GetUsersByProducerID(ProducerID, role))
            {
                if (ds.Tables.Count > 0)
                    // Map data to UserGet class using DataTableMapToList
                    userList = hlpObj.ConvertDataTable<UserGet>(ds.Tables[0]).ToList();
            }
            return userList;
        }

        public User GetUser(int ID)
        {
            PAMDL DBObj = new PAMDL();
            Helper hlpObj = new Helper();

            User usr = new User();
            List<ProducerAdmMapping> PAMList = new List<ProducerAdmMapping>();

            using (DataSet ds = DBObj.GetUser(ID))
            {
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                        foreach (DataRow dr in ds.Tables[0].Rows)
                            foreach (PropertyInfo prop1 in usr.GetType().GetProperties())
                                if (!prop1.Name.Equals("ProducerAdm", StringComparison.InvariantCultureIgnoreCase)
                                        && !prop1.Name.Equals("CreatedBy", StringComparison.InvariantCultureIgnoreCase) && !Equals(dr[prop1.Name], DBNull.Value))
                                    prop1.SetValue(usr, dr[prop1.Name], null);
                                else if (prop1.Name.Equals("ProducerAdm", StringComparison.InvariantCultureIgnoreCase))
                                    if (ds.Tables[1].Rows.Count > 0)
                                        foreach (DataRow drPA in ds.Tables[1].Rows)
                                        {
                                            var objProdAdm = Activator.CreateInstance<ProducerAdmMapping>();
                                            foreach (PropertyInfo prop2 in objProdAdm.GetType().GetProperties())
                                                if (!Equals(drPA[prop2.Name], DBNull.Value))
                                                    prop2.SetValue(objProdAdm, drPA[prop2.Name], null);
                                            PAMList.Add(objProdAdm);
                                        }
                }
                usr.ProducerAdm = PAMList;
            }
            return usr;
        }

        public UserByEmail GetUserByEmail(string emailID)
        {
            PAMDL DBObj = new PAMDL();
            Helper hlpObj = new Helper();

            UserByEmail usr = new UserByEmail();
            List<ProducerAdmMapping> PAMList = new List<ProducerAdmMapping>();

            using (DataSet ds = DBObj.GetUserbyEmail(emailID))
            {
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                        foreach (DataRow dr in ds.Tables[0].Rows)
                            foreach (PropertyInfo prop1 in usr.GetType().GetProperties())
                                if (!prop1.Name.Equals("ProducerAdm", StringComparison.InvariantCultureIgnoreCase)
                                        && !prop1.Name.Equals("CreatedBy", StringComparison.InvariantCultureIgnoreCase) && !Equals(dr[prop1.Name], DBNull.Value))
                                    prop1.SetValue(usr, dr[prop1.Name], null);
                                else if (prop1.Name.Equals("ProducerAdm", StringComparison.InvariantCultureIgnoreCase))
                                    if (ds.Tables[1].Rows.Count > 0)
                                        foreach (DataRow drPA in ds.Tables[1].Rows)
                                        {
                                            var objProdAdm = Activator.CreateInstance<ProducerAdmMapping>();
                                            foreach (PropertyInfo prop2 in objProdAdm.GetType().GetProperties())
                                                if (!Equals(drPA[prop2.Name], DBNull.Value))
                                                    prop2.SetValue(objProdAdm, drPA[prop2.Name], null);
                                            PAMList.Add(objProdAdm);
                                        }
                }
                usr.ProducerAdm = PAMList;
            }
            return usr;
        }

        public List<ProducerAdmMapping> GetProducersForAdmin(string emailID)
        {
            PAMDL DBObj = new PAMDL();
            Helper hlpObj = new Helper();
            
            List<ProducerAdmMapping> PAMList = new List<ProducerAdmMapping>();

            using (DataSet ds = DBObj.GetProducersForAdmin(emailID))
            {
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        { 
                            var objProdAdm = Activator.CreateInstance<ProducerAdmMapping>();
                            foreach (PropertyInfo prop1 in objProdAdm.GetType().GetProperties())
                                    if (!Equals(dr[prop1.Name], DBNull.Value))
                                        prop1.SetValue(objProdAdm, dr[prop1.Name], null);
                            PAMList.Add(objProdAdm);                                        
                        }
                }
                return PAMList;
            }
        }
        public string AddAdministrator(User admin)
        {
            PAMDL DBObj = new PAMDL();
            DataTable dt = new DataTable();

            dt.Columns.Add("ProducerID", typeof(string));
            dt.Columns.Add("FirstName", typeof(string));
            dt.Columns.Add("LastName", typeof(string));

            foreach (var producer in admin.ProducerAdm)
            {
                DataRow dr = dt.NewRow();
                foreach (PropertyInfo prop in producer.GetType().GetProperties())
                {
                    if (!prop.Name.Equals("AdministratorID", StringComparison.InvariantCultureIgnoreCase) &&
                            !prop.Name.Equals("IsActive", StringComparison.InvariantCultureIgnoreCase) &&
                                producer.GetType().GetProperty(prop.Name).GetValue(producer, null) != null)
                        dr[prop.Name] = producer.GetType().GetProperty(prop.Name).GetValue(producer, null);
                }
                dt.Rows.Add(dr);
            }
            string retval = DBObj.AddAdministratortoDB(admin.FirstName, admin.LastName, admin.EmailID, admin.DOB, admin.AgencyName, admin.Commission_Stmt
                                                        ,admin.IsInHongKong, admin.IsInSingapore, admin.IsInBermuda, admin.CreatedBy, dt);

            return retval;
        }

        public string AddTLBStaff(User TLBStaff)
        {
            PAMDL DBObj = new PAMDL();
            string retval = DBObj.AddTLBStafftoDB(TLBStaff.FirstName, TLBStaff.LastName, TLBStaff.EmailID, TLBStaff.DOB, TLBStaff.Commission_Stmt
                                                        , TLBStaff.IsInHongKong, TLBStaff.IsInSingapore, TLBStaff.IsInBermuda, TLBStaff.CreatedBy);

            return retval;
        }
        public string UpdateUser(User user)
        {
            PAMDL DBObj = new PAMDL();
            string retval = DBObj.UpdateUsertoDB(user.ID, user.Commission_Stmt, user.IsInHongKong, user.IsInSingapore, user.IsInBermuda, user.IsActive);

            return retval;
        }

        //public string UpdateTLBStaff(User TLBStaff)
        //{
        //    PAMDL DBObj = new PAMDL();
        //    string retval = DBObj.UpdateTLBStafftoDB(TLBStaff.ID, TLBStaff.Commission_Stmt, TLBStaff.IsInHongKong, TLBStaff.IsInSingapore, TLBStaff.IsInBermuda, TLBStaff.IsActive);

        //    return retval;
        //}

        public string UpdateProducerAdminMapping(List<ProducerAdmMapping> PAMList)
        {
            PAMDL DBObj = new PAMDL();
            DataTable dt = new DataTable();

            dt.Columns.Add("AdministratorID", typeof(int));
            dt.Columns.Add("ProducerID", typeof(string));
            dt.Columns.Add("FirstName", typeof(string));
            dt.Columns.Add("LastName", typeof(string));
            dt.Columns.Add("IsActive", typeof(string));

            foreach (var producer in PAMList)
            {
                DataRow dr = dt.NewRow();
                foreach (PropertyInfo prop in producer.GetType().GetProperties())
                {
                    if (producer.GetType().GetProperty(prop.Name).GetValue(producer, null) != null)
                        dr[prop.Name] = producer.GetType().GetProperty(prop.Name).GetValue(producer, null);
                }
                dt.Rows.Add(dr);
            }
            string retval = DBObj.UpdateProducerAdminMappingtoDB(dt);

            return retval;
        }
    }
}
