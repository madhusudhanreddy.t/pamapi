﻿using BLL;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Security.Principal;
using System.Web.Configuration;
using System.Web.Http;

namespace PAMAPI.Controllers
{
    [AllowAnonymous]
    public class PAMUserController : ApiController
    {
        private static List<T> DataReaderMapToList<T>(DbDataReader dr)
        {
            List<T> list = new List<T>();
            while (dr.Read())
            {
                var obj = Activator.CreateInstance<T>();
                int propertyCounter = 1;
                foreach (PropertyInfo prop in obj.GetType().GetProperties())
                {
                    if (propertyCounter <= dr.FieldCount && !Equals(dr[prop.Name], DBNull.Value))
                    {
                        prop.SetValue(obj, dr[prop.Name], null);
                    }
                    propertyCounter++;
                }
                list.Add(obj);
            }
            return list;
        }

        // GET: api/GetUsers/emailID/Role
        [Route("GetAllUsers")]
        [HttpGet]
        public List<UserGet> GetAllUsers()
        {
            UserMethods BLObj = new UserMethods();

            //WindowsIdentity wi = new WindowsIdentity(User.Identity.Name.Substring(User.Identity.Name.IndexOf("\\") + 1));

            //bool IsInPAMAdminGrp = (from groups in wi.Groups
            //                        where groups.Translate(typeof(NTAccount)).ToString() == WebConfigurationManager.AppSettings["PAMAdminGroup"]
            //                        select groups).Any();

            if (true) //(User.Identity.IsAuthenticated && IsInPAMAdminGrp)
            {
                var userlist = (from users in BLObj.GetAllUsers()
                                orderby users.Role descending
                                select users).ToList();

                if (userlist.Count == 0)
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                    {
                        Content = new StringContent("Users not found"),
                        ReasonPhrase = "Currently there are no Users"
                    };
                    throw new HttpResponseException(resp);
                }
                return userlist;
            }
            else
            {
                var resp = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                {
                    Content = new StringContent("User not authenticated")
                };
                throw new HttpResponseException(resp);
            }
        }

        // GET: api/GetUsersByEmail/emailID/Role
        [Route("GetUsersByEmail/{emailID}/{Role}")]
        [HttpGet]
        public List<UserGet> GetUsersByEmail(string emailID, string role)
        {
            UserMethods BLObj = new UserMethods();

            //WindowsIdentity wi = new WindowsIdentity(User.Identity.Name.Substring(User.Identity.Name.IndexOf("\\") + 1));

            //bool IsInPAMAdminGrp = (from groups in wi.Groups
            //                        where groups.Translate(typeof(NTAccount)).ToString() == WebConfigurationManager.AppSettings["PAMAdminGroup"]
            //                        select groups).Any();

            if (true) //(User.Identity.IsAuthenticated && IsInPAMAdminGrp)
            {
                var userlist = (from users in BLObj.GetUsersByEmail(emailID, role)
                                orderby users.Role descending
                                select users).ToList();

                if (userlist.Count == 0)
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                    {
                        Content = new StringContent("Users not found with given emailID and role"),
                        ReasonPhrase = "Currently there are no Userswith given emailID and role"
                    };
                    throw new HttpResponseException(resp);
                }
                return userlist;
            }
            else
            {
                var resp = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                {
                    Content = new StringContent("User not authenticated")
                };
                throw new HttpResponseException(resp);
            }
        }

        // GET: api/GetUsersByName/firstname/lastname/Role
        [Route("GetUsersByName/{Fname}/{LName}/{Role}")]
        [HttpGet]
        public List<UserGet> GetUsersByName(string Fname, string Lname, string role)
        {
            UserMethods BLObj = new UserMethods();

            //WindowsIdentity wi = new WindowsIdentity(User.Identity.Name.Substring(User.Identity.Name.IndexOf("\\") + 1));

            //bool IsInPAMAdminGrp = (from groups in wi.Groups
            //                        where groups.Translate(typeof(NTAccount)).ToString() == WebConfigurationManager.AppSettings["PAMAdminGroup"]
            //                        select groups).Any();

            if (true) //(User.Identity.IsAuthenticated && IsInPAMAdminGrp)
            {
                var userlist = (from users in BLObj.GetUsersByName(Fname, Lname, role)
                                orderby users.Role descending
                                select users).ToList();

                if (userlist.Count == 0)
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                    {
                        Content = new StringContent("Users not found with given Name and Role"),
                        ReasonPhrase = "Currently there are no Userswith given Name and Role"
                    };
                    throw new HttpResponseException(resp);
                }
                return userlist;
            }
            else
            {
                var resp = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                {
                    Content = new StringContent("User not authenticated")
                };
                throw new HttpResponseException(resp);
            }
        }

        // GET: api/GetUsersByAgencyName/agencyName/Role
        [Route("GetUsersByAgencyName/{agency}/{Role}")]
        [HttpGet]
        public List<UserGet> GetUsersByAgencyName(string agency, string role)
        {
            UserMethods BLObj = new UserMethods();

            //WindowsIdentity wi = new WindowsIdentity(User.Identity.Name.Substring(User.Identity.Name.IndexOf("\\") + 1));

            //bool IsInPAMAdminGrp = (from groups in wi.Groups
            //                        where groups.Translate(typeof(NTAccount)).ToString() == WebConfigurationManager.AppSettings["PAMAdminGroup"]
            //                        select groups).Any();

            if (true) //(User.Identity.IsAuthenticated && IsInPAMAdminGrp)
            {
                var userlist = (from users in BLObj.GetUsersByAgency(agency, role)
                                orderby users.Role descending
                                select users).ToList();

                if (userlist.Count == 0)
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                    {
                        Content = new StringContent("Users not found with given Agency Name and Role"),
                        ReasonPhrase = "Currently there are no Userswith given Agency Name and Role"
                    };
                    throw new HttpResponseException(resp);
                }
                return userlist;
            }
            else
            {
                var resp = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                {
                    Content = new StringContent("User not authenticated")
                };
                throw new HttpResponseException(resp);
            }
        }

        // GET: api/GetUsersByProducerID/producerID/Role
        [Route("GetUsersByProducerID/{producerID}/{Role}")]
        [HttpGet]
        public List<UserGet> GetUsersByProducerID(string producerID, string role)
        {
            UserMethods BLObj = new UserMethods();

            //WindowsIdentity wi = new WindowsIdentity(User.Identity.Name.Substring(User.Identity.Name.IndexOf("\\") + 1));

            //bool IsInPAMAdminGrp = (from groups in wi.Groups
            //                        where groups.Translate(typeof(NTAccount)).ToString() == WebConfigurationManager.AppSettings["PAMAdminGroup"]
            //                        select groups).Any();

            if (true) //(User.Identity.IsAuthenticated && IsInPAMAdminGrp)
            {
                var userlist = (from users in BLObj.GetUsersByProducerID(producerID, role)
                                orderby users.Role descending
                                select users).ToList();

                if (userlist.Count == 0)
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                    {
                        Content = new StringContent("Users not found with given ProducerID and Role"),
                        ReasonPhrase = "Currently there are no users with given ProducerID and Role"
                    };
                    throw new HttpResponseException(resp);
                }
                return userlist;
            }
            else
            {
                var resp = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                {
                    Content = new StringContent("User not authenticated")
                };
                throw new HttpResponseException(resp);
            }
        }

        // GET: api/GetUser/ID
        [Route("GetUser/{ID}")]
        [HttpGet]
        public User GetUser(int ID)
        {
            UserMethods BLObj = new UserMethods();

            //WindowsIdentity wi = new WindowsIdentity(User.Identity.Name.Substring(User.Identity.Name.IndexOf("\\") + 1));

            //bool IsInPAMAdminGrp = (from groups in wi.Groups
            //                        where groups.Translate(typeof(NTAccount)).ToString() == WebConfigurationManager.AppSettings["PAMAdminGroup"]
            //                        select groups).Any();

            if (true) //(User.Identity.IsAuthenticated && IsInPAMAdminGrp)
            {
                return BLObj.GetUser(ID);
            }
            else
            {
                var resp = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                {
                    Content = new StringContent("User not authenticated")
                };
                throw new HttpResponseException(resp);
            }
        }

        [Route("GetUsertoPortal/{emailID}")]
        [HttpGet]
        public UserByEmail GetUsertoPortal(string emailID)
        {
            UserMethods BLObj = new UserMethods();

            //WindowsIdentity wi = new WindowsIdentity(User.Identity.Name.Substring(User.Identity.Name.IndexOf("\\") + 1));

            //bool IsInPAMAdminGrp = (from groups in wi.Groups
            //                        where groups.Translate(typeof(NTAccount)).ToString() == WebConfigurationManager.AppSettings["PAMAdminGroup"]
            //                        select groups).Any();

            if (true) //(User.Identity.IsAuthenticated && IsInPAMAdminGrp)
            {
                return BLObj.GetUserByEmail(emailID);
            }
            else
            {
                var resp = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                {
                    Content = new StringContent("User not authenticated")
                };
                throw new HttpResponseException(resp);
            }
        }

        [Route("GetProducersForAdmin/{emailID}")]
        [HttpGet]
        public List<ProducerAdmMapping> GetProducersForAdmin(string emailID)
        {
            UserMethods BLObj = new UserMethods();

            //WindowsIdentity wi = new WindowsIdentity(User.Identity.Name.Substring(User.Identity.Name.IndexOf("\\") + 1));

            //bool IsInPAMAdminGrp = (from groups in wi.Groups
            //                        where groups.Translate(typeof(NTAccount)).ToString() == WebConfigurationManager.AppSettings["PAMAdminGroup"]
            //                        select groups).Any();

            if (true) //(User.Identity.IsAuthenticated && IsInPAMAdminGrp)
            {
                return BLObj.GetProducersForAdmin(emailID);
            }
            else
            {
                var resp = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                {
                    Content = new StringContent("User not authenticated")
                };
                throw new HttpResponseException(resp);
            }
        }

        // POST: api/AddAdmin
        [Authorize]
        [Route("AddAdmin")]
        [HttpPost]
        public IHttpActionResult PostAdmin([FromBody]User admin)
        {
            IHttpActionResult response;
            UserMethods BLObj = new UserMethods();

            if (!ModelState.IsValid)
            {
                response = ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid data."));
                return response;
            }
            try
            {
                WindowsIdentity wi = new WindowsIdentity(User.Identity.Name.Substring(User.Identity.Name.IndexOf("\\") + 1));

                bool IsInPAMAdminGrp = (from groups in wi.Groups
                                        where groups.Translate(typeof(NTAccount)).ToString() == WebConfigurationManager.AppSettings["PAMAdminGroup"]
                                        select groups).Any();

                if (User.Identity.IsAuthenticated && IsInPAMAdminGrp)
                {
                    if (admin.ProducerAdm == null)
                    {
                        var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                        {
                            Content = new StringContent("Atleast one producer must be mapped to an administrator")
                        };
                        throw new HttpResponseException(resp);
                    }
                    else
                    {
                        admin.CreatedBy = User.Identity.Name;

                        string result = BLObj.AddAdministrator(admin);
                        int respCode;

                        if (result == "Administrator added successfully")
                            respCode = Convert.ToInt32(HttpStatusCode.OK);
                        else
                            respCode = Convert.ToInt32(HttpStatusCode.BadRequest);

                        var responseMsg = Request.CreateResponse((HttpStatusCode)respCode, result);

                        responseMsg.Headers.Location = new Uri(Request.RequestUri.ToString());
                        response = ResponseMessage(responseMsg);

                        return response;
                    }
                }
                else
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                    {
                        Content = new StringContent("User not authenticated")
                    };
                    throw new HttpResponseException(resp);
                }
            }
            catch (SqlException ex)
            {
                if (ex.Message.Contains("UNIQUE KEY constraint"))
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Email already exists for another user. Cannot add user."));
                else
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex));
            }
            catch (HttpResponseException ex)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Response.Content.ReadAsStringAsync().Result));
            }
        }

        // POST: api/AddAdmin
        [Authorize]
        [Route("AddTLBStaff")]
        [HttpPost]
        public IHttpActionResult PostTLBStaff([FromBody]User TLBStaff)
        {
            IHttpActionResult response;
            UserMethods BLObj = new UserMethods();

            if (!ModelState.IsValid)
            {
                response = ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid data."));
                return response;
            }
            try
            {
                WindowsIdentity wi = new WindowsIdentity(User.Identity.Name.Substring(User.Identity.Name.IndexOf("\\") + 1));

                bool IsInPAMAdminGrp = (from groups in wi.Groups
                                        where groups.Translate(typeof(NTAccount)).ToString() == WebConfigurationManager.AppSettings["PAMAdminGroup"]
                                        select groups).Any();

                if (User.Identity.IsAuthenticated && IsInPAMAdminGrp)
                {
                    TLBStaff.CreatedBy = User.Identity.Name;

                    string result = BLObj.AddTLBStaff(TLBStaff);
                    int respCode;

                    if (result == "TLBStaff added successfully")
                        respCode = Convert.ToInt32(HttpStatusCode.OK);
                    else
                        respCode = Convert.ToInt32(HttpStatusCode.BadRequest);

                    var responseMsg = Request.CreateResponse((HttpStatusCode)respCode, result);

                    responseMsg.Headers.Location = new Uri(Request.RequestUri.ToString());
                    response = ResponseMessage(responseMsg);

                    return response;
                }
                else
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                    {
                        Content = new StringContent("User not authenticated")
                    };
                    throw new HttpResponseException(resp);
                }
            }
            catch (SqlException ex)
            {
                if (ex.Message.Contains("UNIQUE KEY constraint"))
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Email already exists for another user. Cannot add user."));
                else
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex));
            }
            catch (HttpResponseException ex)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Response.Content.ReadAsStringAsync().Result));
            }
        }

        // PUT: api/UpdateAccessControl
        [Authorize]
        [Route("UpdateAccessControl/{id}")]
        [HttpPut]
        public IHttpActionResult PutUser(int id, [FromBody]UserAccessControl user)
        {
            IHttpActionResult response;
            UserMethods BLObj = new UserMethods();
            User updatedUser = new User();

            if (!ModelState.IsValid)
            {
                response = ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid data."));
                return response;
            }
            try
            {
                WindowsIdentity wi = new WindowsIdentity(User.Identity.Name.Substring(User.Identity.Name.IndexOf("\\") + 1));

                bool IsInPAMAdminGrp = (from groups in wi.Groups
                                        where groups.Translate(typeof(NTAccount)).ToString() == WebConfigurationManager.AppSettings["PAMAdminGroup"]
                                        select groups).Any();

                if (User.Identity.IsAuthenticated && IsInPAMAdminGrp)
                {
                    updatedUser = BLObj.GetUser(id);

                    if (updatedUser.ID == 0)
                    {
                        var resp = new HttpResponseMessage(HttpStatusCode.NoContent)
                        {
                            Content = new StringContent(string.Format("User with id {0} is not found. ", id)),
                            ReasonPhrase = "Cannot update as user is not found"
                        };
                        throw new HttpResponseException(resp);
                    }
                    else
                    {
                        updatedUser.Commission_Stmt = user.Commission_Stmt;
                        updatedUser.IsInHongKong = user.IsInHongKong;
                        updatedUser.IsInSingapore = user.IsInSingapore;
                        updatedUser.IsInBermuda = user.IsInBermuda;
                        updatedUser.IsActive = user.IsActive;
                    }

                    string result = BLObj.UpdateUser(updatedUser);
                    int respCode;

                    if (result == "User updated successfully")
                        respCode = Convert.ToInt32(HttpStatusCode.OK);
                    else
                        respCode = Convert.ToInt32(HttpStatusCode.BadRequest);

                    var responseMsg = Request.CreateResponse((HttpStatusCode)respCode, result);

                    responseMsg.Headers.Location = new Uri(Request.RequestUri.ToString());
                    response = ResponseMessage(responseMsg);

                    return response;
                }
                else
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                    {
                        Content = new StringContent("User not authenticated")
                    };
                    throw new HttpResponseException(resp);
                }
            }
            catch (HttpResponseException ex)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Response.Content.ReadAsStringAsync().Result));
            }
        }

        // PUT: api/UpdateProducerAdminMapping/id
        [Authorize]
        [Route("UpdatePAM/{id}")]
        [HttpPut]
        public IHttpActionResult PutProducerAdmMapping(int id, [FromBody]List<ProducerAdmMapping> PAMList)
        {
            IHttpActionResult response;
            UserMethods BLObj = new UserMethods();

            if (!ModelState.IsValid)
            {
                response = ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid data."));
                return response;
            }
            try
            {
                WindowsIdentity wi = new WindowsIdentity(User.Identity.Name.Substring(User.Identity.Name.IndexOf("\\") + 1));

                bool IsInPAMAdminGrp = (from groups in wi.Groups
                                        where groups.Translate(typeof(NTAccount)).ToString() == WebConfigurationManager.AppSettings["PAMAdminGroup"]
                                        select groups).Any();

                if (User.Identity.IsAuthenticated && IsInPAMAdminGrp)
                {
                    if (BLObj.GetUser(id).ID == 0)
                    {
                        var resp = new HttpResponseMessage(HttpStatusCode.NoContent)
                        {
                            Content = new StringContent(string.Format("Administrator with id {0} is not found. ", id)),
                            ReasonPhrase = "Cannot update producer(s) as administrator is not found"
                        };
                        throw new HttpResponseException(resp);
                    }
                    else
                    {
                        foreach (var PAM in PAMList)
                            PAM.AdministratorID = id;
                    }

                    string result = BLObj.UpdateProducerAdminMapping(PAMList);
                    int respCode;

                    if (result == "Producer(s) updated successfully")
                        respCode = Convert.ToInt32(HttpStatusCode.OK);
                    else
                        respCode = Convert.ToInt32(HttpStatusCode.BadRequest);

                    var responseMsg = Request.CreateResponse((HttpStatusCode)respCode, result);

                    responseMsg.Headers.Location = new Uri(Request.RequestUri.ToString());
                    response = ResponseMessage(responseMsg);

                    return response;
                }
                else
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                    {
                        Content = new StringContent("User not authenticated")
                    };
                    throw new HttpResponseException(resp);
                }
            }
            catch (HttpResponseException ex)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Response.Content.ReadAsStringAsync().Result));
            }
        }
    }
}
