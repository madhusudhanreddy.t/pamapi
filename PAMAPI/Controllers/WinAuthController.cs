﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.DirectoryServices.AccountManagement;
using System.Security.Principal;
using System.Web.Configuration;

namespace PAMAPI.Controllers
{
    public class WinAuthController : ApiController
    {
        [Authorize]
        [HttpGet]
        [Route("ValidateUser")]
        public IHttpActionResult ValidateUser()
        {
            WindowsIdentity wi = new WindowsIdentity(User.Identity.Name.Substring(User.Identity.Name.IndexOf("\\") + 1));

            bool IsInPAMAdminGrp = (from groups in wi.Groups
                                    where groups.Translate(typeof(NTAccount)).ToString() == WebConfigurationManager.AppSettings["PAMAdminGroup"]
                                    select groups).Any();

            if (User.Identity.IsAuthenticated && IsInPAMAdminGrp)
            {
                return Ok("Authentication successful");
            }
            else
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "You are not authorized to access this application"));
            }
        }

        [HttpGet]
        [Route("GetGroups")]
        public List<GroupPrincipal> GetGroups(string userName)
        {
            List<GroupPrincipal> result = new List<GroupPrincipal>();
            List<string> result1 = new List<string>();

            // establish domain context
            PrincipalContext yourDomain = new PrincipalContext(ContextType.Domain);

            // find your user
            UserPrincipal user = UserPrincipal.FindByIdentity(yourDomain, userName);

            WindowsIdentity wi = new WindowsIdentity(userName);

            bool IsInPAMAdminGrp = (from groups in wi.Groups
                           where groups.Translate(typeof(NTAccount)).ToString() == WebConfigurationManager.AppSettings["PAMAdminGroup"]
                                    select groups).Any();
            foreach (IdentityReference group in wi.Groups)
            {
                
                try
                {
                    result1.Add(group.Translate(typeof(NTAccount)).ToString());
                }
                catch (Exception ex) { }
            }
            result1.Sort();
            //return result1;

            // if found - grab its groups
            if (user != null)
            {
                PrincipalSearchResult<Principal> groups = user.GetAuthorizationGroups();

                // iterate over all groups
                foreach (Principal p in groups)
                {
                    // make sure to add only group principals
                    if (p is GroupPrincipal)
                    {
                        result.Add((GroupPrincipal)p);
                    }
                }
            }
            return result;
        }
    }
}
